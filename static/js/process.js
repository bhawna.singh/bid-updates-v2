title = document.getElementById('title')
progress_bar = document.getElementById('progress_bar')

function update(percentage, new_title) {
	if (isNaN(percentage) || !new_title)
		return
	title.innerText = new_title
	if (percentage < 0) {
		progress_bar.hidden = true
		return
	}
	progress_bar.ariaValueNow = percentage
	progress_bar.style.width = percentage + '%'
}

task_id = document.currentScript.getAttribute('task_id')
json_post = JSON.stringify({task_id: task_id})
function sync_title() {
	(async() => {
		route = window.origin+'/process/percentage/'
		response = await fetch(route, {
			method: "POST",
			body: json_post,
			headers: new Headers({"content-type": "application/json"})
		})
		json = await response.json()
		update(json.percentage, json.title)
	})().catch(err => title.innerText = update(-1, 'ERROR'))
}

(async () => {
	t = setInterval(sync_title, 10000)
	route = window.origin+'/process/start/'
	let response = await fetch(route, {
		method: "POST",
		body: json_post,
		headers: new Headers({"content-type": "application/json"})
	})
	text = await response.text()
	// console.log('response.text(): ')
	// console.log(text)
	clearInterval(t)
	// if (response.) {}
	// document.documentElement.innerHTML =
	window.location.replace('/success/'+text)
})().catch(err => title.innerText = update(-1, 'ERROR'))
