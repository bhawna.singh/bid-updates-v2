import pandas as pd
import numpy as np
import xlsxwriter
import warnings
warnings.filterwarnings('ignore')
import configparser
from common import constants, common_modules
import os,glob,logging



@common_modules.MeasureTime
def get_uploaded_files(upload_folder, session_id):
    user_files={}
    user_dataframe={}
    custom_file={}
    upload_folder_fullpath = os.path.join(os.getcwd(), upload_folder)
    current_files = glob.glob(upload_folder_fullpath + '/*')
    for files in current_files:
        id = files.split("/")[-1].split("_")[0]
        key = files.split("/")[-1].split("_")[1]
        if (id == session_id) & (key == 'file45'):
            user_files['file45'] = files
        if (id == session_id) & (key == 'file7'):
            user_files['file7'] = files
        if (id == session_id) & (key == 'custom'):
            custom_file['custom'] = files
    logging.info("User data files created '{0}'".format(user_files))
    global sheets
    for key, value in user_files.items():
        xls = pd.ExcelFile(value)
        sheets = [x.replace(' ', '') for x in xls.sheet_names]
        if 'SponsoredProductsCampaigns' and 'SponsoredBrandsCampaigns' in sheets:
            products = pd.read_excel(xls, sheet_name='Sponsored Products Campaigns')
            if len(products) > 0:
                is_European0 = products[products['Max Bid'].apply(common_modules.is_european_format)][
                                   'Max Bid'].count() > 0
                if is_European0:
                    products['ACoS'] = products['ACoS'].astype(str).str.replace(',', '.').astype(str).str.rstrip(
                        '%').astype('float') / 100.0
                else:
                    products['ACoS'] = products['ACoS'].astype(str).str.rstrip('%').astype('float') / 100.0
            else:
                pass
            brands = pd.read_excel(xls, sheet_name='Sponsored Brands Campaigns')
            if len(brands) > 0:
                is_European1 = brands[brands['Max Bid'].apply(common_modules.is_european_format)]['Max Bid'].count() > 0
                if is_European1:
                    brands['ACoS'] = brands['ACoS'].astype(str).str.replace(',', '.').astype(str).str.rstrip(
                        '%').astype('float') / 100.0
                else:
                    brands['ACoS'] = brands['ACoS'].astype(str).str.rstrip('%').astype('float') / 100.0
            else:
                pass
            user_dataframe[key] = [products, brands]

        elif 'SponsoredBrandsCampaigns1' and 'SponsoredBrandsCampaigns2' and 'SponsoredProductsCampaigns' in sheets:
            products = pd.read_excel(xls, sheet_name='Sponsored Products Campaigns')
            if len(products) > 0:
                is_European0 = products[products['Max Bid'].apply(common_modules.is_european_format)][
                                   'Max Bid'].count() > 0
                if is_European0:
                    products['ACoS'] = products['ACoS'].astype(str).str.replace(',', '.').astype(str).str.rstrip(
                        '%').astype('float') / 100.0
                else:
                    products['ACoS'] = products['ACoS'].astype(str).str.rstrip('%').astype('float') / 100.0
            else:
                pass
            brands_1 = pd.read_excel(xls, sheet_name='Sponsored Brands Campaigns 1')
            if len(brands_1) > 0:
                is_European1 = brands_1[brands_1['Max Bid'].apply(common_modules.is_european_format)][
                                   'Max Bid'].count() > 0
                if is_European1:
                    brands_1['ACoS'] = brands_1['ACoS'].astype(str).str.replace(',', '.').astype(str).str.rstrip(
                        '%').astype('float') / 100.0
                else:
                    brands_1['ACoS'] = brands_1['ACoS'].astype(str).str.rstrip('%').astype('float') / 100.0
            else:
                pass
            brands_2 = pd.read_excel(xls, sheet_name='Sponsored Brands Campaigns 2')
            if len(brands_2) > 0:
                is_European1 = brands_2[brands_2['Max Bid'].apply(common_modules.is_european_format)][
                                   'Max Bid'].count() > 0
                if is_European1:
                    brands_2['ACoS'] = brands_2['ACoS'].astype(str).str.replace(',', '.').astype(str).str.rstrip(
                        '%').astype('float') / 100.0
                else:
                    brands_2['ACoS'] = brands_2['ACoS'].astype(str).str.rstrip('%').astype('float') / 100.0
            else:
                pass
            user_dataframe[key] = [products, brands_1, brands_2]

        elif 'SponsoredProductsCampaigns1' and 'SponsoredProductsCampaigns2' and 'SponsoredBrandsCampaigns' in sheets:
            products_1 = pd.read_excel(xls, sheet_name='Sponsored Products Campaigns 1')
            if len(products_1) > 0:
                is_European1 = products_1[products_1['Max Bid'].apply(common_modules.is_european_format)][
                                   'Max Bid'].count() > 0
                if is_European1:
                    products_1['ACoS'] = products_1['ACoS'].astype(str).str.replace(',', '.').astype(str).str.rstrip(
                        '%').astype('float') / 100.0
                else:
                    products_1['ACoS'] = products_1['ACoS'].astype(str).str.rstrip('%').astype('float') / 100.0
            else:
                pass
            products_2 = pd.read_excel(xls, sheet_name='Sponsored Products Campaigns 2')
            if len(products_2) > 0:
                is_European1 = products_2[products_2['Max Bid'].apply(common_modules.is_european_format)][
                                   'Max Bid'].count() > 0
                if is_European1:
                    products_2['ACoS'] = products_2['ACoS'].astype(str).str.replace(',', '.').astype(str).str.rstrip(
                        '%').astype('float') / 100.0
                else:
                    products_2['ACoS'] = products_2['ACoS'].astype(str).str.rstrip('%').astype('float') / 100.0
            else:
                pass
            brands = pd.read_excel(xls, sheet_name='Sponsored Brands Campaigns')
            if len(brands) > 0:
                is_European1 = brands[brands['Max Bid'].apply(common_modules.is_european_format)]['Max Bid'].count() > 0
                if is_European1:
                    brands['ACoS'] = brands['ACoS'].astype(str).str.replace(',', '.').astype(str).str.rstrip(
                        '%').astype('float') / 100.0
                else:
                    brands['ACoS'] = brands['ACoS'].astype(str).str.rstrip('%').astype('float') / 100.0
            else:
                pass
            user_dataframe[key] = [products_1, products_2, brands]

        elif 'SponsoredProductsCampaigns1' and 'SponsoredProductsCampaigns2' and 'SponsoredBrandsCampaigns1' and 'SponsoredBrandsCampaigns2' in sheets:
            products_1 = pd.read_excel(xls, sheet_name='Sponsored Products Campaigns 1')
            if len(products_1) > 0:
                is_European1 = products_1[products_1['Max Bid'].apply(common_modules.is_european_format)][
                                   'Max Bid'].count() > 0
                if is_European1:
                    products_1['ACoS'] = products_1['ACoS'].astype(str).str.replace(',', '.').astype(str).str.rstrip(
                        '%').astype('float') / 100.0
                else:
                    products_1['ACoS'] = products_1['ACoS'].astype(str).str.rstrip('%').astype('float') / 100.0
            else:
                pass
            products_2 = pd.read_excel(xls, sheet_name='Sponsored Products Campaigns 2')
            if len(products_2) > 0:
                is_European1 = products_2[products_2['Max Bid'].apply(common_modules.is_european_format)][
                                   'Max Bid'].count() > 0
                if is_European1:
                    products_2['ACoS'] = products_2['ACoS'].astype(str).str.replace(',', '.').astype(str).str.rstrip(
                        '%').astype('float') / 100.0
                else:
                    products_2['ACoS'] = products_2['ACoS'].astype(str).str.rstrip('%').astype('float') / 100.0
            else:
                pass
            brands_1 = pd.read_excel(xls, sheet_name='Sponsored Brands Campaigns 1')
            if len(brands_1) > 0:
                is_European1 = brands_1[brands_1['Max Bid'].apply(common_modules.is_european_format)][
                                   'Max Bid'].count() > 0
                if is_European1:
                    brands_1['ACoS'] = brands_1['ACoS'].astype(str).str.replace(',', '.').astype(str).str.rstrip(
                        '%').astype('float') / 100.0
                else:
                    brands_1['ACoS'] = brands_1['ACoS'].astype(str).str.rstrip('%').astype('float') / 100.0
            else:
                pass
            brands_2 = pd.read_excel(xls, sheet_name='Sponsored Brands Campaigns 2')
            if len(brands_2) > 0:
                is_European1 = brands_2[brands_2['Max Bid'].apply(common_modules.is_european_format)][
                                   'Max Bid'].count() > 0
                if is_European1:
                    brands_2['ACoS'] = brands_2['ACoS'].astype(str).str.replace(',', '.').astype(str).str.rstrip(
                        '%').astype('float') / 100.0
                else:
                    brands_2['ACoS'] = brands_2['ACoS'].astype(str).str.rstrip('%').astype('float') / 100.0
            else:
                pass
            user_dataframe[key] = [products_1, products_2, brands_1, brands_2]
        else:
            products = pd.DataFrame()
            brands = pd.DataFrame()
        os.remove(value)
        logging.info("Successfully deleted '{0}'".format(key))


    if len(custom_file)>0:
       custom_dataframe = pd.read_excel(custom_file['custom'])
       if len(custom_dataframe)>0:
           is_European2 = custom_dataframe[custom_dataframe['Target_ACoS'].apply(common_modules.is_european_format)]['Target_ACoS'].count() > 0
           if is_European2:
               custom_dataframe['Target_ACoS'] = custom_dataframe['Target_ACoS'].astype(str).str.replace(',',   '.').astype(str).str.rstrip('%').astype('float') / 100.0
           else:
               custom_dataframe['Target_ACoS'] = custom_dataframe['Target_ACoS'].astype(str).str.rstrip('%').astype('float')

       #custom_dataframe['Target_ACoS'] = custom_dataframe['Target_ACoS'].astype('float64')
           custom_dataframe.drop('Custom', axis=1, inplace=True)
           os.remove(custom_file['custom'])
           logging.info("Successfully deleted custom file '{0}'".format(custom_file['custom']))
       #print(custom_dataframe.head())
           return user_dataframe, custom_dataframe, sheets
       else:
           custom_dataframe = pd.DataFrame()
           return user_dataframe, custom_dataframe, sheets
    else:
        custom_dataframe = pd.DataFrame()
        return user_dataframe, custom_dataframe, sheets


@common_modules.MeasureTime
def max_bid_products(df, record_types, status):
    df = df[(df['Record Type'].str.lower().isin(record_types))] #Filtering Record types namely:Campaign, Campaign Placement and Ad
    df = df[~((df['Status'].str.lower().isin(status))|(df['Ad Group Status'].str.lower().isin(status))|(df['Campaign Status'].str.lower().isin(status)))] #Filtering the paused and rejected status types
    df = df[~(df['Match Type'].str.lower().str.contains("negative",na=False))] #Filtering records that contain negative phrase in column Match
    #df = df.loc[~((df['Impressions'] == 0) & (df['Clicks'] == 0) & (df['Spend'] == 0) & (df['Orders'] == 0) & (df['Sales'] == 0) & (df['ACoS'] == 0))]
    df['Max Bid'] = df.groupby(['Campaign ID', 'Ad Group'])['Max Bid'].ffill() #Forward filling the max bid by grouping the campaign Id and Ad group Id
    df['Campaign ID']=df['Campaign ID'].astype(str)
    return df

@common_modules.MeasureTime
def max_bid_brands(df, record_types, status):
    df = df[(df['Record Type'].str.lower().isin(record_types))] #Filtering Record types namely:Campaign, Campaign Placement and Ad
    df = df[~((df['Status'].str.lower().isin(status))|(df['Campaign Status'].str.lower().isin(status)))] #Filtering the paused and rejected status types
    df = df[(df['Ad Group Status'].isnull())]
    #print('brands sheet with status draft',np.where(df['Status']=='draft'))
    df = df[~(df['Match Type'].str.lower().str.contains("negative",na=False))] #Filtering records that contain negative phrase in column Match
    #df = df.loc[~((df['Impressions']==0)&(df['Clicks']==0)&(df['Spend']==0)&(df['Orders']==0)&(df['Sales']==0)&(df['ACoS']==0))]
    df['Max Bid'].fillna(0.25,inplace=True) #Forward filling the max bid by grouping the campaign Id and Ad group Id
    df['Campaign ID']=df['Campaign ID'].astype(str)
    return df

@common_modules.MeasureTime
def get_column_index(df_products, df_brands, sheets,col_name, session_id):
    config = configparser.ConfigParser()
    download_folder_fullpath = os.path.join(os.getcwd(), constants.download_folder)
    column_mappings_products = {}
    column_mappings_brands = {}
    for col in col_name:
        if len(df_products)>0:
            index_no_products = df_products.columns.get_loc(col)                        #getting location of mentioned column name
            col_index_products = xlsxwriter.utility.xl_col_to_name(index_no_products)
            column_mappings_products[col] = col_index_products
        else:
            column_mappings_products['Warning Message']="No products campaigns sheet was uploaded"
        if len(df_brands) > 0:
            index_no_brands = df_brands.columns.get_loc(col)
            col_index_brands = xlsxwriter.utility.xl_col_to_name(index_no_brands)
            column_mappings_brands[col] = col_index_brands
        else:
            column_mappings_brands['Warning Message'] = "No Brands campaigns sheet was uploaded"
    config[sheets[0]] = column_mappings_products
    config[sheets[1]] = column_mappings_brands
    with open(os.path.join(download_folder_fullpath, str(session_id)+'.ini'), 'w') as configfile:      #data into new ini file
        config.write(configfile)

@common_modules.MeasureTime
def determine_target_acos_products(d1, custom, user_input):
    if len(custom)>0:
        custom_products = custom.rename(columns={'Keyword': 'Keyword or Product Targeting'})
        c1 = custom_products.loc[custom_products['Campaign'].notnull() &
                                 custom_products['Ad Group'].notnull()
                                 & custom_products['Keyword or Product Targeting'].notnull() &
                                 custom_products['Match Type'].notnull()]
        df_merged = pd.merge(d1, c1, on=['Campaign', 'Ad Group', 'Keyword or Product Targeting',
                                         'Match Type'], how='left')

        c2 = custom_products.loc[custom_products['Campaign'].notnull() &
                                 custom_products['Ad Group'].notnull()
                                 & custom_products['Keyword or Product Targeting'].notnull() &
                                 custom_products['Match Type'].isnull()]
        df_merged.loc[df_merged['Target_ACoS'].isna(), 'Target_ACoS'] = pd.merge(df_merged, c2,
                                        on=['Campaign', 'Ad Group',
                                            'Keyword or Product Targeting'], how='left')['Target_ACoS_x']

        c3 = custom_products.loc[custom_products['Campaign'].notnull() &
                                     custom_products['Ad Group'].notnull() &
                                     custom_products['Keyword or Product Targeting'].isnull() &
                                     custom_products['Match Type'].isnull()]
        df_merged.loc[df_merged['Target_ACoS'].isna(), 'Target_ACoS'] = pd.merge(df_merged,
                                                c3, on=['Campaign', 'Ad Group'],
                                                                    how='left')['Target_ACoS_x']

        c4 = custom_products.loc[custom_products['Campaign'].notnull() &
                                     custom_products['Ad Group'].isnull() &
                                     custom_products['Keyword or Product Targeting'].isnull() &
                                     custom_products['Match Type'].isnull()]
        x = df_merged.merge(c4, on=['Campaign'], how='left')
        x.loc[x['Target_ACoS_x'].isna(), 'Target_ACoS_x'] = x['Target_ACoS_y']
        x.drop(['Target_ACoS_y'], axis = 1, inplace = True)
        y = df_merged.loc[df_merged['Target_ACoS'].isna(), 'Target_ACoS']
        y = x
        y.drop(['Ad Group_y','Keyword or Product Targeting_y','Match Type_y','Record Type_y','Record Type'], axis =1, inplace = True)
        y = y.rename(columns ={'Ad Group_x' : 'Ad Group', 'Keyword or Product Targeting_x' : 'Keyword or Product   Targeting', 'Match Type_x' : 'Match Type','Target_ACoS_x' :'Target_ACoS','Record Type_x' : 'Record Type'})
       # df_merged.loc[df_merged['Target_ACoS'].isna(), 'Target_ACoS'] = pd.merge(df_merged,
       #                                         c4, on=['Campaign'], how='left')['Target_ACoS_y']

        y['Target_ACoS'].fillna(user_input, inplace=True)

        return y
    else:
        d1['Target_ACoS'] = 0
        d1['Target_ACoS'] = user_input
        return d1

@common_modules.MeasureTime
def determine_target_acos_brands(d1, custom, user_input):
    if len(custom)>0:
        c1 = custom.loc[custom['Campaign'].notnull() &
                                 custom['Ad Group'].notnull()
                                 & custom['Keyword'].notnull() &
                                 custom['Match Type'].notnull()]
        df_merged = pd.merge(d1, c1, on=['Campaign', 'Ad Group', 'Keyword', 'Match Type'], how='left')

        c2 = custom.loc[custom['Campaign'].notnull() &
                                 custom['Ad Group'].notnull()
                                 & custom['Keyword'].notnull() &
                                 custom['Match Type'].isnull()]
        df_merged.loc[df_merged['Target_ACoS'].isna(), 'Target_ACoS'] = pd.merge(df_merged, c2,
                                        on=['Campaign', 'Ad Group', 'Keyword'],
                                                                        how='left')['Target_ACoS_x']

        c3 = custom.loc[custom['Campaign'].notnull() &
                                     custom['Ad Group'].notnull() &
                                     custom['Keyword'].isnull() &
                                     custom['Match Type'].isnull()]
        df_merged.loc[df_merged['Target_ACoS'].isna(), 'Target_ACoS'] = pd.merge(df_merged,
                                                c3, on=['Campaign', 'Ad Group'],
                                                                    how='left')['Target_ACoS_x']

        c4 = custom.loc[custom['Campaign'].notnull() &
                                     custom['Ad Group'].isnull() &
                                     custom['Keyword'].isnull() &
                                     custom['Match Type'].isnull()]
        x = df_merged.merge(c4, on=['Campaign'], how='left')   
        x.loc[x['Target_ACoS_x'].isna(), 'Target_ACoS_x'] = x['Target_ACoS_y']
        x.drop(['Target_ACoS_y'], axis = 1, inplace = True)
        y = df_merged.loc[df_merged['Target_ACoS'].isna(), 'Target_ACoS']
        y = x
        y.drop(['Ad Group_y','Keyword_y','Match Type_y','Record Type_y','Record Type'], axis =1, inplace = True)
        y = y.rename(columns ={'Ad Group_x' : 'Ad Group', 'Keyword_x' : 'Keyword', 'Match Type_x' : 'Match Type','Target_ACoS_x' :'Target_ACoS', 'Record Type_x' : 'Record Type'})
       # df_merged.loc[df_merged['Target_ACoS'].isna(), 'Target_ACoS'] = pd.merge(df_merged,
                          #                      c4, on=['Campaign'], how='left')['Target_ACoS_y']
        y['Target_ACoS'].fillna(user_input, inplace=True)

        return y
    else:
        d1['Target_ACoS'] = user_input
        return d1

#claasifying each row into tiers based on orders, acos, impressions, spend

def set_bid_tier(orders, acos, impressions, spend, target_acos, default_data,user_input):
    # condition for tier_under_invested
    if ((orders >= user_input['under_invested']) & (acos <= target_acos)):
        return 'tier_under_invested'

    # condition for tier_on_target
    elif ((orders >= user_input['under_invested']) &
          (acos > target_acos) &
          (acos <= target_acos + default_data['category_bound_on_target'])):
        return 'tier_on_target'

    # condition for tier_poorperf-1
    elif ((spend >= user_input['poor_performer']) &
          (acos > target_acos + default_data['category_bound_on_target']) &
          (acos <= target_acos + default_data['category_bound_poorperf_1'])):
        return 'tier_poorperf-1'

    # condition for tier_poorperf-2
    elif ((spend >= user_input['poor_performer']) &
          (acos > target_acos + default_data['category_bound_poorperf_1']) &
          (acos <= target_acos + default_data['category_bound_poorperf_2'])):
        return 'tier_poorperf-2'

    # condition for tier_poorperf-3
    elif ((spend >= user_input['poor_performer']) &
          (acos > target_acos + default_data['category_bound_poorperf_2']) &
          (acos <= target_acos + default_data['category_bound_poorperf_3'])):
        return 'tier_poorperf-3'

    # condition for tier_poorperf-4
    elif (((spend >= user_input['poor_performer']) &
           (acos > target_acos + default_data['category_bound_poorperf_3'])) |
          ((spend >= user_input['poor_performer']) &
           (orders == 0))):
        return 'tier_poorperf-4'

    # condition for tier_not_validated
    elif ((orders > 0) &
          (orders < user_input['under_invested']) &
          (acos <= target_acos )):
        return 'tier_not_validated'

    # condition for tier_low_traffic-1
    elif ((impressions > 0) &
          (impressions < user_input['low_traffic'])):
        return 'tier_low_traffic-1'

    # condition for tier_low_traffic-2
    elif (impressions == 0):
        return 'tier_low_traffic-2'

    # condition for tier_no_change
    elif (impressions != 0):
        return 'tier_no_change'

    # If none of the condition works
    else:
        return 'Not Calculated'


def calculate_bid(tier, user_input):
    if tier == 'tier_under_invested':
        return user_input['under_invested_bidadjust']+1
    elif tier == 'tier_on_target':
        return user_input['on_target_bidadjust']+1
    elif tier == 'tier_poorperf-1':
        return user_input['poor_performer_1_bidadjust']+1
    elif tier == 'tier_poorperf-2':
        return user_input['poor_performer_2_bidadjust']+1
    elif tier == 'tier_poorperf-3':
        return user_input['poor_performer_3_bidadjust']+1
    elif tier == 'tier_poorperf-4':
        return user_input['poor_performer_4_bidadjust']+1
    elif tier == 'tier_not_validated':
        return user_input['not_validate_bidadjust']+1
    elif tier == 'tier_low_traffic-1':
        return user_input['low_traffic_1_bidadjust']+1
    elif tier == 'tier_low_traffic-2':
        return user_input['low_traffic_2_bidadjust']+1
    elif tier == 'tier_no_change':
        return 1
    else:
        return 1

@common_modules.MeasureTime
def fetch_45day_details(df_updated, df_45):
    df_45 = df_45.filter(['Record ID', 'Record Type', 'Clicks', 'Spend', 'ACoS'], axis=1)
    is_European = df_45[df_45['Spend'].apply(common_modules.is_european_format)]['Spend'].count() > 0
    if is_European:
        df_45 = common_modules.comma2decimal(df_45, ['Spend'])
    df_45['Spend'] = df_45['Spend'].astype('float64')
    df_updated = df_updated.merge(df_45, on=['Record ID'], how='left').rename(
                                            columns={'Record Type_x': 'Record Type',
                                                        'ACoS_y': 'Canopy_45_Day_ACoS',
                                                        'ACoS_x': 'ACoS',
                                                        'Clicks_x': 'Clicks',
                                                        'Spend_x': 'Spend'})
    df_updated['Canopy_45_Day_CPC'] = round((df_updated['Spend_y'] / df_updated['Clicks_y']),2)
    #df_updated['Canopy_45_Day_CPC'].fillna('No Clicks', inplace=True)
    df_updated.drop(['Clicks_y', 'Spend_y', 'Record Type_y'], axis=1, inplace=True)
    df_updated = df_updated.loc[:, ~df_updated.columns.duplicated()]
    df_updated['Record ID']=df_updated['Record ID'].astype(str)
    return df_updated

@common_modules.MeasureTime
def new_bid_calculation(df_updated,user_input):
    df_updated['Canopy_Overoptimized'] = 'No'
    df_updated['Canopy_45_Day_7_Day_CPC'] = 0
    df = df_updated.loc[df_updated['Canopy_Bid_Tier'].isin(constants.poor_performer)]
    if len(df) > 0:
        category_bound_over_optimized = constants.default_data['category_bound_over_optimized']
        over_optimized_bidadjust = user_input['over_optimized_bidadjust']
        df['calculated_bid'] = round((df['Canopy_Original_Bid'] * (over_optimized_bidadjust+1)), 2)
        cpc_7 = round(df['Spend'] / df['Clicks'], 2)
        df['Max Bid'] = df.apply(lambda x: x['calculated_bid'] if 0 < x['Canopy_45_Day_ACoS'] < (x['Target_ACoS']+category_bound_over_optimized) else x['Max Bid'] , axis =1)
        df['Canopy_Overoptimized'] = df.apply(lambda x: 'Yes' if 0 < x['Canopy_45_Day_ACoS'] < (x['Target_ACoS']+category_bound_over_optimized) else 'No', axis =1)
        df['Canopy_45_Day_7_Day_CPC'] = round((df['Canopy_45_Day_CPC'] - cpc_7).astype('float'),2)
        df_updated['Canopy_45_Day_CPC'].fillna('No Clicks', inplace=True)
        df_updated['Canopy_Overoptimized'] = df['Canopy_Overoptimized']
        df_updated['Canopy_45_Day_7_Day_CPC'] = df['Canopy_45_Day_7_Day_CPC']
        df_updated['calculated_bid'] = df['Max Bid']
        df_updated['Max Bid'] = df_updated.apply(lambda x: x['calculated_bid'] if x['Canopy_Overoptimized'] == 'Yes' else x['Max Bid'], axis=1)
        df_updated.drop('calculated_bid', axis=1, inplace=True)
        return df_updated
    else:
        return df_updated

@common_modules.MeasureTime
def write_file(user_dataframe,session_id):
    download_folder_fullpath = os.path.join(os.getcwd(), constants.download_folder)
    writer_2 = pd.ExcelWriter(os.path.join(download_folder_fullpath, str(session_id)+str('.xlsx')), engine="xlsxwriter")
    #user_dataframe['file7'][0].to_excel(writer_2, sheet_name='Portfolios', index=False, header=True)  # sheet 1
    workbook_2 = writer_2.book
    fmt_number = workbook_2.add_format({"num_format": "0"})
    sheets = get_uploaded_files(constants.upload_folder, session_id)
    sheets = sheets[2]
    if 'SponsoredProductsCampaigns' and 'SponsoredBrandsCampaigns' in sheets:
        user_dataframe['file7'][0].drop_duplicates(subset ="Record ID",keep = 'first', inplace = True)
        user_dataframe['file7'][0].to_excel(writer_2, sheet_name='Sponsored Products Campaigns', encoding='utf-8',index=False, header=True)  # sheet 2
        worksheet_2 = writer_2.sheets['Sponsored Products Campaigns']
        worksheet_2.set_column("A:A", 20, fmt_number)
        worksheet_2.set_column("C:C", 20, fmt_number)
        user_dataframe['file7'][1].drop_duplicates(subset ="Record ID",keep = 'first', inplace = True)
        user_dataframe['file7'][1].to_excel(writer_2, sheet_name='Sponsored Brands Campaigns', encoding='utf-8',index=False, header=True)  # sheet 3
        worksheet_3 = writer_2.sheets['Sponsored Brands Campaigns']
        worksheet_3.set_column("A:A", 25, fmt_number)
        worksheet_3.set_column("C:C", 25, fmt_number)
    elif 'SponsoredBrandsCampaigns1' and 'SponsoredBrandsCampaigns2' and 'SponsoredProductsCampaigns' in sheets:
        user_dataframe['file7'][0].drop_duplicates(subset ="Record ID",keep = 'first', inplace = True)
        user_dataframe['file7'][0].to_excel(writer_2, sheet_name='Sponsored Products Campaigns', encoding='utf-8',index=False, header=True)  # sheet 2
        worksheet_2 = writer_2.sheets['Sponsored Products Campaigns']
        worksheet_2.set_column("A:A", 20, fmt_number)
        worksheet_2.set_column("C:C", 20, fmt_number)
        user_dataframe['file7'][1].drop_duplicates(subset ="Record ID",keep = 'first', inplace = True)
        user_dataframe['file7'][1].to_excel(writer_2, sheet_name='Sponsored Brands Campaigns 1', encoding='utf-8',index=False, header=True)  # sheet 3
        worksheet_4 = writer_2.sheets['Sponsored Brands Campaigns 1']
        worksheet_4.set_column("A:A", 25, fmt_number)
        worksheet_4.set_column("C:C", 25, fmt_number)
        user_dataframe['file7'][2].drop_duplicates(subset ="Record ID",keep = 'first', inplace = True)
        user_dataframe['file7'][2].to_excel(writer_2, sheet_name='Sponsored Brands Campaigns 2', encoding='utf-8',index=False, header=True)  # sheet 3
        worksheet_5 = writer_2.sheets['Sponsored Brands Campaigns 2']
        worksheet_5.set_column("A:A", 25, fmt_number)
        worksheet_5.set_column("C:C", 25, fmt_number)
    elif 'SponsoredProductsCampaigns1' and 'SponsoredProductsCampaigns2' and 'SponsoredBrandsCampaigns' in sheets:
        user_dataframe['file7'][0].drop_duplicates(subset ="Record ID",keep = 'first', inplace = True)
        user_dataframe['file7'][0].to_excel(writer_2, sheet_name='Sponsored Products Campaigns 1', encoding='utf-8',index=False, header=True)  # sheet 2
        worksheet_2 = writer_2.sheets['Sponsored Products Campaigns 1']
        worksheet_2.set_column("A:A", 20, fmt_number)
        worksheet_2.set_column("C:C", 20, fmt_number)
        user_dataframe['file7'][1].drop_duplicates(subset ="Record ID",keep = 'first', inplace = True)
        user_dataframe['file7'][1].to_excel(writer_2, sheet_name='Sponsored Products Campaigns 2', encoding='utf-8',index=False, header=True)  # sheet 2
        worksheet_3 = writer_2.sheets['Sponsored Products Campaigns 2']
        worksheet_3.set_column("A:A", 20, fmt_number)
        worksheet_3.set_column("C:C", 20, fmt_number)
        user_dataframe['file7'][2].drop_duplicates(subset ="Record ID",keep = 'first', inplace = True)
        user_dataframe['file7'][2].to_excel(writer_2, sheet_name='Sponsored Brands Campaigns', encoding='utf-8',index=False, header=True)  # sheet 3
        worksheet_3 = writer_2.sheets['Sponsored Brands Campaigns']
        worksheet_3.set_column("A:A", 25, fmt_number)
        worksheet_3.set_column("C:C", 25, fmt_number)
    elif 'SponsoredProductsCampaigns1' and 'SponsoredProductsCampaigns2' and 'SponsoredBrandsCampaigns1' and 'SponsoredBrandsCampaigns2' in sheets:
        user_dataframe['file7'][0].drop_duplicates(subset ="Record ID",keep = 'first', inplace = True)
        user_dataframe['file7'][0].to_excel(writer_2, sheet_name='Sponsored Products Campaigns 1', encoding='utf-8',index=False, header=True)  # sheet 2
        worksheet_2 = writer_2.sheets['Sponsored Products Campaigns 1']
        worksheet_2.set_column("A:A", 20, fmt_number)
        worksheet_2.set_column("C:C", 20, fmt_number)
        user_dataframe['file7'][1].drop_duplicates(subset ="Record ID",keep = 'first', inplace = True)
        user_dataframe['file7'][1].to_excel(writer_2, sheet_name='Sponsored Products Campaigns 2', encoding='utf-8',index=False, header=True)  # sheet 2
        worksheet_3 = writer_2.sheets['Sponsored Products Campaigns 2']
        worksheet_3.set_column("A:A", 20, fmt_number)
        worksheet_3.set_column("C:C", 20, fmt_number)
        user_dataframe['file7'][2].drop_duplicates(subset ="Record ID",keep = 'first', inplace = True)
        user_dataframe['file7'][2].to_excel(writer_2, sheet_name='Sponsored Brands Campaigns 1', encoding='utf-8', index=False, header=True)  # sheet 3
        worksheet_4 = writer_2.sheets['Sponsored Brands Campaigns 1']
        worksheet_4.set_column("A:A", 25, fmt_number)
        worksheet_4.set_column("C:C", 25, fmt_number)
        user_dataframe['file7'][3].drop_duplicates(subset ="Record ID",keep = 'first', inplace = True)
        user_dataframe['file7'][3].to_excel(writer_2, sheet_name='Sponsored Brands Campaigns 2', encoding='utf-8',index=False, header=True)  # sheet 3
        worksheet_5 = writer_2.sheets['Sponsored Brands Campaigns 2']
        worksheet_5.set_column("A:A", 25, fmt_number)
        worksheet_5.set_column("C:C", 25, fmt_number)

    writer_2.save()

@common_modules.MeasureTime
def get_downloaded_files(download_folder, session_id):
    updated_files={}
    download_folder_fullpath = os.path.join(os.getcwd(), download_folder)
    current_files = glob.glob(download_folder_fullpath + '/*')
    for files in current_files:
        id = files.split("/")[-1].split(".")[0]
        ext = files.split(".")[-1]
        #print(ext)
        #print("ID is", id)
        #print("ext is", ext)
        if (id == session_id) & (ext == 'xlsx'):
            updated_files['file7'] = files
        if (id == session_id) & (ext == 'ini'):
            updated_files['ini'] = files
    return updated_files

@common_modules.MeasureTime
def max_bid_bound(df, lower_bound, user_input):
    df['Max Bid'] = np.where(df['Max Bid']< lower_bound, lower_bound,df['Max Bid'])
    if (user_input['upper_bound'])=='':
        pass
    else:
        df['Max Bid'] = np.where(df['Max Bid'] > user_input['upper_bound'], user_input['upper_bound'], df['Max Bid'])
    df['Max Bid'] = round(df['Max Bid'],2)
    return df

@common_modules.MeasureTime
def top_level_flow(task_id, user_input):
    #print("Here is the input of user",user_input)
    common_modules.update_processing_tasks(task_id, 'Loading the files', 5)
    user_dataframe,custom_dataframe, sheets  = get_uploaded_files(constants.upload_folder, task_id)
    common_modules.update_processing_tasks(task_id, 'Processing multiple tabs in files', 20)
    get_column_index(user_dataframe['file7'][0],user_dataframe['file7'][1], constants.sheets, constants.col_name, task_id)
    common_modules.update_processing_tasks(task_id, 'Config File Created', 30)
    if 'SponsoredProductsCampaigns' and 'SponsoredBrandsCampaigns' in sheets:
        if len(user_dataframe['file7'][0]) > 0:
            is_European0 = user_dataframe['file7'][0][user_dataframe['file7'][0]['Max Bid'].apply(common_modules.is_european_format)][
                'Max Bid'].count() > 0
            if is_European0:
                user_dataframe['file7'][0] = common_modules.comma2decimal(user_dataframe['file7'][0],
                                                                          ['Max Bid', 'Sales', 'Spend'])
                for columns in ['Max Bid', 'Sales', 'Spend']:
                    user_dataframe['file7'][0][columns] = user_dataframe['file7'][0][columns].astype('float64')
            else:
                pass

            if len(user_dataframe['file7'][0]) > 0:
                user_dataframe['file7'][0] = max_bid_products(user_dataframe['file7'][0],
                                                              constants.record_types_product,
                                                              constants.status)

                # print("Level 1 Max Bid Initial", user_dataframe['file7'][0]['Max Bid'])
                user_dataframe['file7'][0] = determine_target_acos_products(user_dataframe['file7'][0],
                                                                            custom_dataframe, user_input['target_acos'])
                func1 = np.vectorize(set_bid_tier)

                user_dataframe['file7'][0]['Canopy_Bid_Tier'] = func1(user_dataframe['file7'][0]['Orders'],
                                                                      user_dataframe['file7'][0]['ACoS'],
                                                                      user_dataframe['file7'][0]['Impressions'],
                                                                      user_dataframe['file7'][0]['Spend'],
                                                                      user_dataframe['file7'][0]['Target_ACoS'],
                                                                      constants.default_data, user_input)

                # print("Level 2 Bid Tier", user_dataframe['file7'][0].loc[user_dataframe['file7'][0]['Canopy_Bid_Tier'].isin(constants.poor_performer)]['Max Bid'].value_counts())
                common_modules.update_processing_tasks(task_id, 'Classifying each row into Bid Tier', 50)
                user_dataframe['file7'][0]['Canopy_Original_Bid'] = user_dataframe['file7'][0]['Max Bid']
                func2 = np.vectorize(calculate_bid)
                print("check here", user_dataframe['file7'][0]['Max Bid'].dtypes)
                print("check type", user_dataframe['file7'][0]['Canopy_Bid_Tier'].dtypes)
                user_dataframe['file7'][0]['Max Bid'] = user_dataframe['file7'][0].apply(
                    lambda x: x['Max Bid'] * calculate_bid(x['Canopy_Bid_Tier'], user_input), axis=1)

                # print("Level 3 Bid Calculate", user_dataframe['file7'][0].loc[user_dataframe['file7'][0]['Canopy_Bid_Tier'].isin(constants.poor_performer)]['Max Bid'].value_counts())
                # print("USer input is", user_input)

                # print("Level 4 New column Max Bid Calculate", user_dataframe['file7'][0].loc[user_dataframe['file7'][0]['Canopy_Bid_Tier'].isin(constants.poor_performer)]['Max Bid'].value_counts())

                if (len(user_dataframe['file45'][0]) > 0):
                    user_dataframe['file7'][0] = fetch_45day_details(user_dataframe['file7'][0],
                                                                     user_dataframe['file45'][0])
                    common_modules.update_processing_tasks(task_id, 'Fetching ACOS and CPC from 45 day file ', 60)
                else:
                    user_dataframe['file7'][0]['Canopy_45_Day_CPC'] = 0
                    user_dataframe['file7'][0]['Canopy_45_Day_ACoS'] = 0

                    # user_dataframe['file7'][0]['Max Bid'] = user_dataframe['file7'][0]['Max Bid']
                user_dataframe['file7'][0] = new_bid_calculation(user_dataframe['file7'][0], user_input)

                # print((user_dataframe['file7'][0]['Max Bid']<=0.02).sum())
                user_dataframe['file7'][0] = max_bid_bound(user_dataframe['file7'][0], constants.lower_bound_products,
                                                           user_input)
                user_dataframe['file7'][0]['Bid Variance'] = (user_dataframe['file7'][0]['Max Bid'] -user_dataframe['file7'][0]['Canopy_Original_Bid']) / user_dataframe['file7'][0]['Canopy_Original_Bid']
                user_dataframe['file7'][0]['Bid Variance'] = round(user_dataframe['file7'][0]['Bid Variance'], 2)
                user_dataframe['file7'][0].loc[~np.isfinite(user_dataframe['file7'][0]['Bid Variance']), 'Bid Variance'] = 'Original Bid is 0'
                # print((user_dataframe['file7'][0]['Max Bid']==0.02).sum())

            else:
                pass

            if is_European0:
                if len(user_dataframe['file7'][0]) > 0:
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column].astype('float64')
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column] * 100
                        user_dataframe['file7'][0][column] = round(user_dataframe['file7'][0][column], 2)
                    user_dataframe['file7'][0] = common_modules.decimal2comma(user_dataframe['file7'][0],
                                                                              ['Max Bid', 'Sales', 'Spend', 'ACoS',
                                                                               'Target_ACoS',
                                                                               'Canopy_Original_Bid',
                                                                               'Canopy_45_Day_ACoS',
                                                                               'Canopy_45_Day_CPC',
                                                                               'Canopy_45_Day_7_Day_CPC'])
                    user_dataframe['file7'][0]['Canopy_45_Day_7_Day_CPC'] = user_dataframe['file7'][0][
                        'Canopy_45_Day_7_Day_CPC'].str.replace('nan', ' ')
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column].astype('str') + "%"
                else:
                    pass
            else:
                if len(user_dataframe['file7'][0]) > 0:
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column].astype('float64')
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column] * 100
                        user_dataframe['file7'][0][column] = round(user_dataframe['file7'][0][column], 2)
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column].astype('str') + "%"
                else:
                    pass

        else:
            pass

        if len(user_dataframe['file7'][1]) > 0:
            is_European1 = user_dataframe['file7'][1][user_dataframe['file7'][1]['Max Bid'].apply(common_modules.is_european_format)][
                'Max Bid'].count() > 0
            if is_European1:
                user_dataframe['file7'][1] = common_modules.comma2decimal(user_dataframe['file7'][1],
                                                                          ['Max Bid', 'Sales', 'Spend'])
                for columns in ['Max Bid', 'Sales', 'Spend']:
                    user_dataframe['file7'][1][columns] = user_dataframe['file7'][1][columns].astype('float64')
            else:
                pass
            user_dataframe['file7'][1] = max_bid_brands(user_dataframe['file7'][1], constants.record_types_product,
                                                        constants.status)

            if len(user_dataframe['file7'][1]) > 0:
                user_dataframe['file7'][1] = determine_target_acos_brands(user_dataframe['file7'][1], custom_dataframe,
                                                                          user_input['target_acos'])
                func1 = np.vectorize(set_bid_tier)

                user_dataframe['file7'][1]['Canopy_Bid_Tier'] = func1(user_dataframe['file7'][1]['Orders'],
                                                                      user_dataframe['file7'][1]['ACoS'],
                                                                      user_dataframe['file7'][1]['Impressions'],
                                                                      user_dataframe['file7'][1]['Spend'],
                                                                      user_dataframe['file7'][1]['Target_ACoS'],
                                                                      constants.default_data, user_input)
                user_dataframe['file7'][1]['Canopy_Original_Bid'] = user_dataframe['file7'][1]['Max Bid']

                func2 = np.vectorize(calculate_bid)
                user_dataframe['file7'][1]['Max Bid'] = user_dataframe['file7'][1].apply(
                    lambda x: x['Max Bid'] * calculate_bid(x['Canopy_Bid_Tier'], user_input), axis=1)
                if (len(user_dataframe['file45'][1]) > 0):
                    user_dataframe['file7'][1] = fetch_45day_details(user_dataframe['file7'][1],
                                                                     user_dataframe['file45'][1])
                else:
                    user_dataframe['file7'][1]['Canopy_45_Day_CPC'] = 0
                    user_dataframe['file7'][1]['Canopy_45_Day_ACoS'] = 0

                user_dataframe['file7'][1] = new_bid_calculation(user_dataframe['file7'][1], user_input)

                user_dataframe['file7'][1] = user_dataframe['file7'][1].loc[
                    user_dataframe['file7'][1]['Record Type'] == 'Keyword']
                user_dataframe['file7'][1] = max_bid_bound(user_dataframe['file7'][1], constants.lower_bound_brands,
                                                           user_input)
                user_dataframe['file7'][1]['Bid Variance'] = (user_dataframe['file7'][1]['Max Bid'] -user_dataframe['file7'][1]['Canopy_Original_Bid']) / user_dataframe['file7'][1]['Canopy_Original_Bid']
                user_dataframe['file7'][1]['Bid Variance'] = round(user_dataframe['file7'][1]['Bid Variance'], 2)
                user_dataframe['file7'][1].loc[~np.isfinite(user_dataframe['file7'][1]['Bid Variance']), 'Bid Variance'] = 'Original Bid is 0'
            else:
                pass

            if is_European1:
                if len(user_dataframe['file7'][1]) > 0:
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column].astype('float64')
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column] * 100
                        user_dataframe['file7'][1][column] = round(user_dataframe['file7'][1][column], 2)
                    user_dataframe['file7'][1] = common_modules.decimal2comma(user_dataframe['file7'][1],
                                                                              ['Max Bid', 'Sales', 'Spend', 'ACoS',
                                                                               'Target_ACoS',
                                                                               'Canopy_Original_Bid',
                                                                               'Canopy_45_Day_ACoS',
                                                                               'Canopy_45_Day_CPC',
                                                                               'Canopy_45_Day_7_Day_CPC'])
                    user_dataframe['file7'][1]['Canopy_45_Day_7_Day_CPC'] = user_dataframe['file7'][1][
                        'Canopy_45_Day_7_Day_CPC'].str.replace('nan', ' ')
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column].astype('str') + "%"
                else:
                    pass
            else:
                if len(user_dataframe['file7'][1]) > 0:
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column].astype('float64')
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column] * 100
                        user_dataframe['file7'][1][column] = round(user_dataframe['file7'][1][column], 2)
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column].astype('str') + "%"
                else:
                    pass
        else:
            pass

    elif 'SponsoredBrandsCampaigns1' and 'SponsoredBrandsCampaigns2' and 'SponsoredProductsCampaigns' in sheets:
        if len(user_dataframe['file7'][0]) > 0:
            is_European0 = user_dataframe['file7'][0][user_dataframe['file7'][0]['Max Bid'].apply(common_modules.is_european_format)][
                'Max Bid'].count() > 0
            if is_European0:
                user_dataframe['file7'][0] = common_modules.comma2decimal(user_dataframe['file7'][0],
                                                                          ['Max Bid', 'Sales', 'Spend'])
                for columns in ['Max Bid', 'Sales', 'Spend']:
                    user_dataframe['file7'][0][columns] = user_dataframe['file7'][0][columns].astype('float64')
            else:
                pass

            if len(user_dataframe['file7'][0]) > 0:
                user_dataframe['file7'][0] = max_bid_products(user_dataframe['file7'][0],
                                                              constants.record_types_product,
                                                              constants.status)

                # print("Level 1 Max Bid Initial", user_dataframe['file7'][0]['Max Bid'])
                user_dataframe['file7'][0] = determine_target_acos_products(user_dataframe['file7'][0],
                                                                            custom_dataframe, user_input['target_acos'])
                func1 = np.vectorize(set_bid_tier)

                user_dataframe['file7'][0]['Canopy_Bid_Tier'] = func1(user_dataframe['file7'][0]['Orders'],
                                                                      user_dataframe['file7'][0]['ACoS'],
                                                                      user_dataframe['file7'][0]['Impressions'],
                                                                      user_dataframe['file7'][0]['Spend'],
                                                                      user_dataframe['file7'][0]['Target_ACoS'],
                                                                      constants.default_data, user_input)

                # print("Level 2 Bid Tier", user_dataframe['file7'][0].loc[user_dataframe['file7'][0]['Canopy_Bid_Tier'].isin(constants.poor_performer)]['Max Bid'].value_counts())
                common_modules.update_processing_tasks(task_id, 'Classifying each row into Bid Tier', 50)
                user_dataframe['file7'][0]['Canopy_Original_Bid'] = user_dataframe['file7'][0]['Max Bid']
                func2 = np.vectorize(calculate_bid)
                user_dataframe['file7'][0]['Max Bid'] = user_dataframe['file7'][0].apply(
                    lambda x: x['Max Bid'] * calculate_bid(x['Canopy_Bid_Tier'], user_input), axis=1)

                # print("Level 3 Bid Calculate", user_dataframe['file7'][0].loc[user_dataframe['file7'][0]['Canopy_Bid_Tier'].isin(constants.poor_performer)]['Max Bid'].value_counts())
                # print("USer input is", user_input)

                # print("Level 4 New column Max Bid Calculate", user_dataframe['file7'][0].loc[user_dataframe['file7'][0]['Canopy_Bid_Tier'].isin(constants.poor_performer)]['Max Bid'].value_counts())

                if (len(user_dataframe['file45'][0]) > 0):
                    user_dataframe['file7'][0] = fetch_45day_details(user_dataframe['file7'][0],
                                                                     user_dataframe['file45'][0])
                    common_modules.update_processing_tasks(task_id, 'Fetching ACOS and CPC from 45 day file ', 60)
                else:
                    user_dataframe['file7'][0]['Canopy_45_Day_CPC'] = 0
                    user_dataframe['file7'][0]['Canopy_45_Day_ACoS'] = 0

                    # user_dataframe['file7'][0]['Max Bid'] = user_dataframe['file7'][0]['Max Bid']
                user_dataframe['file7'][0] = new_bid_calculation(user_dataframe['file7'][0], user_input)

                # print((user_dataframe['file7'][0]['Max Bid']<=0.02).sum())
                user_dataframe['file7'][0] = max_bid_bound(user_dataframe['file7'][0], constants.lower_bound_products,
                                                           user_input)
                user_dataframe['file7'][0]['Bid Variance'] = (user_dataframe['file7'][0]['Max Bid'] -user_dataframe['file7'][0]['Canopy_Original_Bid']) / user_dataframe['file7'][0]['Canopy_Original_Bid']
                user_dataframe['file7'][0]['Bid Variance'] = round(user_dataframe['file7'][0]['Bid Variance'], 2)
                user_dataframe['file7'][0].loc[~np.isfinite(user_dataframe['file7'][0]['Bid Variance']), 'Bid Variance'] = 'Original Bid is 0'
                # print((user_dataframe['file7'][0]['Max Bid']==0.02).sum())

            else:
                pass

            if is_European0:
                if len(user_dataframe['file7'][0]) > 0:
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column].astype('float64')
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column] * 100
                        user_dataframe['file7'][0][column] = round(user_dataframe['file7'][0][column], 2)
                    user_dataframe['file7'][0] = common_modules.decimal2comma(user_dataframe['file7'][0],
                                                                              ['Max Bid', 'Sales', 'Spend', 'ACoS',
                                                                               'Target_ACoS',
                                                                               'Canopy_Original_Bid',
                                                                               'Canopy_45_Day_ACoS',
                                                                               'Canopy_45_Day_CPC',
                                                                               'Canopy_45_Day_7_Day_CPC'])
                    user_dataframe['file7'][0]['Canopy_45_Day_7_Day_CPC'] = user_dataframe['file7'][0][
                        'Canopy_45_Day_7_Day_CPC'].str.replace('nan', ' ')
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column].astype('str') + "%"
                else:
                    pass
            else:
                if len(user_dataframe['file7'][0]) > 0:
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column].astype('float64')
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column] * 100
                        user_dataframe['file7'][0][column] = round(user_dataframe['file7'][0][column], 2)
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column].astype('str') + "%"
                else:
                    pass

        else:
            pass
        if len(user_dataframe['file7'][1]) > 0:
            is_European1 = user_dataframe['file7'][1][user_dataframe['file7'][1]['Max Bid'].apply(common_modules.is_european_format)][
                'Max Bid'].count() > 0
            if is_European1:
                user_dataframe['file7'][1] = common_modules.comma2decimal(user_dataframe['file7'][1],
                                                                          ['Max Bid', 'Sales', 'Spend'])
                user_dataframe['file7'][2] = common_modules.comma2decimal(user_dataframe['file7'][2],
                                                                          ['Max Bid', 'Sales', 'Spend'])
                for columns in ['Max Bid', 'Sales', 'Spend']:
                    user_dataframe['file7'][1][columns] = user_dataframe['file7'][1][columns].astype('float64')
                    user_dataframe['file7'][2][columns] = user_dataframe['file7'][2][columns].astype('float64')
            else:
                pass
            user_dataframe['file7'][1] = max_bid_brands(user_dataframe['file7'][1], constants.record_types_product,
                                                        constants.status)
            user_dataframe['file7'][2] = max_bid_brands(user_dataframe['file7'][2], constants.record_types_product,
                                                        constants.status)

            if len(user_dataframe['file7'][1]) > 0:
                user_dataframe['file7'][1] = determine_target_acos_brands(user_dataframe['file7'][1], custom_dataframe,
                                                                          user_input['target_acos'])
                user_dataframe['file7'][2] = determine_target_acos_brands(user_dataframe['file7'][2], custom_dataframe,
                                                                          user_input['target_acos'])
                func1 = np.vectorize(set_bid_tier)

                user_dataframe['file7'][1]['Canopy_Bid_Tier'] = func1(user_dataframe['file7'][1]['Orders'],
                                                                      user_dataframe['file7'][1]['ACoS'],
                                                                      user_dataframe['file7'][1]['Impressions'],
                                                                      user_dataframe['file7'][1]['Spend'],
                                                                      user_dataframe['file7'][1]['Target_ACoS'],
                                                                      constants.default_data, user_input)
                user_dataframe['file7'][2]['Canopy_Bid_Tier'] = func1(user_dataframe['file7'][2]['Orders'],
                                                                      user_dataframe['file7'][2]['ACoS'],
                                                                      user_dataframe['file7'][2]['Impressions'],
                                                                      user_dataframe['file7'][2]['Spend'],
                                                                      user_dataframe['file7'][2]['Target_ACoS'],
                                                                      constants.default_data, user_input)
                user_dataframe['file7'][1]['Canopy_Original_Bid'] = user_dataframe['file7'][1]['Max Bid']
                user_dataframe['file7'][2]['Canopy_Original_Bid'] = user_dataframe['file7'][2]['Max Bid']

                func2 = np.vectorize(calculate_bid)
                user_dataframe['file7'][1]['Max Bid'] = user_dataframe['file7'][1].apply(
                    lambda x: x['Max Bid'] * calculate_bid(x['Canopy_Bid_Tier'], user_input), axis=1)
                user_dataframe['file7'][2]['Max Bid'] = user_dataframe['file7'][2].apply(
                    lambda x: x['Max Bid'] * calculate_bid(x['Canopy_Bid_Tier'], user_input), axis=1)
                if (len(user_dataframe['file45'][1]) > 0):
                    user_dataframe['file7'][1] = fetch_45day_details(user_dataframe['file7'][1],
                                                                     user_dataframe['file45'][1])
                    user_dataframe['file7'][2] = fetch_45day_details(user_dataframe['file7'][2],
                                                                     user_dataframe['file45'][2])

                else:
                    user_dataframe['file7'][1]['Canopy_45_Day_CPC'] = 0
                    user_dataframe['file7'][1]['Canopy_45_Day_ACoS'] = 0
                    user_dataframe['file7'][2]['Canopy_45_Day_CPC'] = 0
                    user_dataframe['file7'][2]['Canopy_45_Day_ACoS'] = 0

                user_dataframe['file7'][1] = new_bid_calculation(user_dataframe['file7'][1], user_input)
                user_dataframe['file7'][2] = new_bid_calculation(user_dataframe['file7'][2], user_input)

                user_dataframe['file7'][1] = user_dataframe['file7'][1].loc[
                    user_dataframe['file7'][1]['Record Type'] == 'Keyword']
                user_dataframe['file7'][2] = user_dataframe['file7'][2].loc[
                    user_dataframe['file7'][2]['Record Type'] == 'Keyword']
                user_dataframe['file7'][1] = max_bid_bound(user_dataframe['file7'][1], constants.lower_bound_brands,
                                                           user_input)
                user_dataframe['file7'][2] = max_bid_bound(user_dataframe['file7'][2], constants.lower_bound_brands,
                                                           user_input)
                user_dataframe['file7'][1]['Bid Variance'] = (user_dataframe['file7'][1]['Max Bid'] -user_dataframe['file7'][1]['Canopy_Original_Bid']) / user_dataframe['file7'][1]['Canopy_Original_Bid']
                user_dataframe['file7'][2]['Bid Variance'] = (user_dataframe['file7'][2]['Max Bid'] -user_dataframe['file7'][2]['Canopy_Original_Bid']) / user_dataframe['file7'][2]['Canopy_Original_Bid']
                user_dataframe['file7'][1]['Bid Variance'] = round(user_dataframe['file7'][1]['Bid Variance'], 2)
                user_dataframe['file7'][2]['Bid Variance'] = round(user_dataframe['file7'][2]['Bid Variance'], 2)
                user_dataframe['file7'][1].loc[~np.isfinite(user_dataframe['file7'][1]['Bid Variance']), 'Bid Variance'] = 'Original Bid is 0'
                user_dataframe['file7'][2].loc[~np.isfinite(user_dataframe['file7'][2]['Bid Variance']), 'Bid Variance'] = 'Original Bid is 0'
            else:
                pass

            if is_European1:
                if len(user_dataframe['file7'][1]) > 0:
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column].astype('float64')
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column] * 100
                        user_dataframe['file7'][1][column] = round(user_dataframe['file7'][1][column], 2)
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column].astype('float64')
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column] * 100
                        user_dataframe['file7'][2][column] = round(user_dataframe['file7'][2][column], 2)
                    user_dataframe['file7'][1] = common_modules.decimal2comma(user_dataframe['file7'][1],
                                                                              ['Max Bid', 'Sales', 'Spend', 'ACoS',
                                                                               'Target_ACoS',
                                                                               'Canopy_Original_Bid',
                                                                               'Canopy_45_Day_ACoS',
                                                                               'Canopy_45_Day_CPC',
                                                                               'Canopy_45_Day_7_Day_CPC'])
                    user_dataframe['file7'][2] = common_modules.decimal2comma(user_dataframe['file7'][2],
                                                                              ['Max Bid', 'Sales', 'Spend', 'ACoS',
                                                                               'Target_ACoS',
                                                                               'Canopy_Original_Bid',
                                                                               'Canopy_45_Day_ACoS',
                                                                               'Canopy_45_Day_CPC',
                                                                               'Canopy_45_Day_7_Day_CPC'])
                    user_dataframe['file7'][1]['Canopy_45_Day_7_Day_CPC'] = user_dataframe['file7'][1][
                        'Canopy_45_Day_7_Day_CPC'].str.replace('nan', ' ')
                    user_dataframe['file7'][2]['Canopy_45_Day_7_Day_CPC'] = user_dataframe['file7'][2][
                        'Canopy_45_Day_7_Day_CPC'].str.replace('nan', ' ')
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column].astype('str') + "%"
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column].astype('str') + "%"
                else:
                    pass
            else:
                if len(user_dataframe['file7'][1]) > 0:
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column].astype('float64')
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column] * 100
                        user_dataframe['file7'][1][column] = round(user_dataframe['file7'][1][column], 2)
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column].astype('str') + "%"
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column].astype('float64')
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column] * 100
                        user_dataframe['file7'][2][column] = round(user_dataframe['file7'][2][column], 2)
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column].astype('str') + "%"
                else:
                    pass
        else:
            pass

    elif 'SponsoredProductsCampaigns1' and 'SponsoredProductsCampaigns2' and 'SponsoredBrandsCampaigns' in sheets:
        if len(user_dataframe['file7'][0]) > 0:
            is_European0 = user_dataframe['file7'][0][user_dataframe['file7'][0]['Max Bid'].apply(common_modules.is_european_format)][
                'Max Bid'].count() > 0
            if is_European0:
                user_dataframe['file7'][0] = common_modules.comma2decimal(user_dataframe['file7'][0],
                                                                          ['Max Bid', 'Sales', 'Spend'])
                user_dataframe['file7'][1] = common_modules.comma2decimal(user_dataframe['file7'][1],
                                                                          ['Max Bid', 'Sales', 'Spend'])
                for columns in ['Max Bid', 'Sales', 'Spend']:
                    user_dataframe['file7'][0][columns] = user_dataframe['file7'][0][columns].astype('float64')
                    user_dataframe['file7'][1][columns] = user_dataframe['file7'][1][columns].astype('float64')
            else:
                pass

            if len(user_dataframe['file7'][0]) > 0:
                user_dataframe['file7'][0] = max_bid_products(user_dataframe['file7'][0],
                                                              constants.record_types_product,
                                                              constants.status)
                user_dataframe['file7'][1] = max_bid_products(user_dataframe['file7'][1],
                                                              constants.record_types_product,
                                                              constants.status)

                # print("Level 1 Max Bid Initial", user_dataframe['file7'][0]['Max Bid'])
                user_dataframe['file7'][0] = determine_target_acos_products(user_dataframe['file7'][0],
                                                                            custom_dataframe, user_input['target_acos'])
                user_dataframe['file7'][1] = determine_target_acos_products(user_dataframe['file7'][1],
                                                                            custom_dataframe, user_input['target_acos'])
                func1 = np.vectorize(set_bid_tier)

                user_dataframe['file7'][0]['Canopy_Bid_Tier'] = func1(user_dataframe['file7'][0]['Orders'],
                                                                      user_dataframe['file7'][0]['ACoS'],
                                                                      user_dataframe['file7'][0]['Impressions'],
                                                                      user_dataframe['file7'][0]['Spend'],
                                                                      user_dataframe['file7'][0]['Target_ACoS'],
                                                                      constants.default_data, user_input)
                user_dataframe['file7'][1]['Canopy_Bid_Tier'] = func1(user_dataframe['file7'][1]['Orders'],
                                                                      user_dataframe['file7'][1]['ACoS'],
                                                                      user_dataframe['file7'][1]['Impressions'],
                                                                      user_dataframe['file7'][1]['Spend'],
                                                                      user_dataframe['file7'][1]['Target_ACoS'],
                                                                      constants.default_data, user_input)

                # print("Level 2 Bid Tier", user_dataframe['file7'][0].loc[user_dataframe['file7'][0]['Canopy_Bid_Tier'].isin(constants.poor_performer)]['Max Bid'].value_counts())
                common_modules.update_processing_tasks(task_id, 'Classifying each row into Bid Tier', 50)
                user_dataframe['file7'][0]['Canopy_Original_Bid'] = user_dataframe['file7'][0]['Max Bid']
                user_dataframe['file7'][1]['Canopy_Original_Bid'] = user_dataframe['file7'][1]['Max Bid']
                func2 = np.vectorize(calculate_bid)
                user_dataframe['file7'][0]['Max Bid'] = user_dataframe['file7'][0].apply(
                    lambda x: x['Max Bid'] * calculate_bid(x['Canopy_Bid_Tier'], user_input), axis=1)
                user_dataframe['file7'][1]['Max Bid'] = user_dataframe['file7'][1].apply(
                    lambda x: x['Max Bid'] * calculate_bid(x['Canopy_Bid_Tier'], user_input), axis=1)

                # print("Level 3 Bid Calculate", user_dataframe['file7'][0].loc[user_dataframe['file7'][0]['Canopy_Bid_Tier'].isin(constants.poor_performer)]['Max Bid'].value_counts())
                # print("USer input is", user_input)

                # print("Level 4 New column Max Bid Calculate", user_dataframe['file7'][0].loc[user_dataframe['file7'][0]['Canopy_Bid_Tier'].isin(constants.poor_performer)]['Max Bid'].value_counts())

                if (len(user_dataframe['file45'][0]) > 0):
                    user_dataframe['file7'][0] = fetch_45day_details(user_dataframe['file7'][0],
                                                                     user_dataframe['file45'][0])
                    user_dataframe['file7'][1] = fetch_45day_details(user_dataframe['file7'][1],
                                                                     user_dataframe['file45'][1])
                    common_modules.update_processing_tasks(task_id, 'Fetching ACOS and CPC from 45 day file ', 60)
                else:
                    user_dataframe['file7'][0]['Canopy_45_Day_CPC'] = 0
                    user_dataframe['file7'][0]['Canopy_45_Day_ACoS'] = 0
                    user_dataframe['file7'][1]['Canopy_45_Day_CPC'] = 0
                    user_dataframe['file7'][1]['Canopy_45_Day_ACoS'] = 0

                # user_dataframe['file7'][0]['Max Bid'] = user_dataframe['file7'][0]['Max Bid']
                user_dataframe['file7'][0] = new_bid_calculation(user_dataframe['file7'][0], user_input)
                user_dataframe['file7'][1] = new_bid_calculation(user_dataframe['file7'][1], user_input)

                # print((user_dataframe['file7'][0]['Max Bid']<=0.02).sum())
                user_dataframe['file7'][0] = max_bid_bound(user_dataframe['file7'][0], constants.lower_bound_products,
                                                           user_input)
                user_dataframe['file7'][1] = max_bid_bound(user_dataframe['file7'][1], constants.lower_bound_products,
                                                           user_input)
                user_dataframe['file7'][0]['Bid Variance'] = (user_dataframe['file7'][0]['Max Bid'] -user_dataframe['file7'][0]['Canopy_Original_Bid']) / user_dataframe['file7'][0]['Canopy_Original_Bid']
                user_dataframe['file7'][1]['Bid Variance'] = (user_dataframe['file7'][1]['Max Bid'] - user_dataframe['file7'][1]['Canopy_Original_Bid']) / user_dataframe['file7'][1]['Canopy_Original_Bid']
                user_dataframe['file7'][0]['Bid Variance'] = round(user_dataframe['file7'][0]['Bid Variance'], 2)
                user_dataframe['file7'][1]['Bid Variance'] = round(user_dataframe['file7'][1]['Bid Variance'], 2)
                user_dataframe['file7'][0].loc[~np.isfinite(user_dataframe['file7'][0]['Bid Variance']), 'Bid Variance'] = 'Original Bid is 0'
                user_dataframe['file7'][1].loc[~np.isfinite(user_dataframe['file7'][1]['Bid Variance']), 'Bid Variance'] = 'Original Bid is 0'
                # print((user_dataframe['file7'][0]['Max Bid']==0.02).sum())

            else:
                pass

            if is_European0:
                if len(user_dataframe['file7'][0]) > 0:
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column].astype('float64')
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column] * 100
                        user_dataframe['file7'][0][column] = round(user_dataframe['file7'][0][column], 2)
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column].astype('float64')
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column] * 100
                        user_dataframe['file7'][1][column] = round(user_dataframe['file7'][1][column], 2)
                    user_dataframe['file7'][0] = common_modules.decimal2comma(user_dataframe['file7'][0],
                                                                              ['Max Bid', 'Sales', 'Spend', 'ACoS',
                                                                               'Target_ACoS',
                                                                               'Canopy_Original_Bid',
                                                                               'Canopy_45_Day_ACoS',
                                                                               'Canopy_45_Day_CPC',
                                                                               'Canopy_45_Day_7_Day_CPC'])
                    user_dataframe['file7'][1] = common_modules.decimal2comma(user_dataframe['file7'][1],
                                                                              ['Max Bid', 'Sales', 'Spend', 'ACoS',
                                                                               'Target_ACoS',
                                                                               'Canopy_Original_Bid',
                                                                               'Canopy_45_Day_ACoS',
                                                                               'Canopy_45_Day_CPC',
                                                                               'Canopy_45_Day_7_Day_CPC'])
                    user_dataframe['file7'][0]['Canopy_45_Day_7_Day_CPC'] = user_dataframe['file7'][0][
                        'Canopy_45_Day_7_Day_CPC'].str.replace('nan', ' ')
                    user_dataframe['file7'][1]['Canopy_45_Day_7_Day_CPC'] = user_dataframe['file7'][1][
                        'Canopy_45_Day_7_Day_CPC'].str.replace('nan', ' ')

                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column].astype('str') + "%"
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column].astype('str') + "%"
                else:
                    pass
            else:
                if len(user_dataframe['file7'][0]) > 0:
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column].astype('float64')
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column] * 100
                        user_dataframe['file7'][0][column] = round(user_dataframe['file7'][0][column], 2)
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column].astype('str') + "%"
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column].astype('float64')
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column] * 100
                        user_dataframe['file7'][1][column] = round(user_dataframe['file7'][1][column], 2)
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column].astype('str') + "%"
                else:
                    pass

        else:
            pass
        if len(user_dataframe['file7'][2]) > 0:
            is_European1 = user_dataframe['file7'][2][user_dataframe['file7'][2]['Max Bid'].apply(common_modules.is_european_format)][
                'Max Bid'].count() > 0
            if is_European1:
                user_dataframe['file7'][2] = common_modules.comma2decimal(user_dataframe['file7'][2],
                                                                          ['Max Bid', 'Sales', 'Spend'])
                for columns in ['Max Bid', 'Sales', 'Spend']:
                    user_dataframe['file7'][2][columns] = user_dataframe['file7'][2][columns].astype('float64')
            else:
                pass
            user_dataframe['file7'][2] = max_bid_brands(user_dataframe['file7'][2], constants.record_types_product,
                                                        constants.status)

            if len(user_dataframe['file7'][2]) > 0:
                user_dataframe['file7'][2] = determine_target_acos_brands(user_dataframe['file7'][2], custom_dataframe,
                                                                          user_input['target_acos'])
                func1 = np.vectorize(set_bid_tier)

                user_dataframe['file7'][2]['Canopy_Bid_Tier'] = func1(user_dataframe['file7'][2]['Orders'],
                                                                      user_dataframe['file7'][2]['ACoS'],
                                                                      user_dataframe['file7'][2]['Impressions'],
                                                                      user_dataframe['file7'][2]['Spend'],
                                                                      user_dataframe['file7'][2]['Target_ACoS'],
                                                                      constants.default_data, user_input)
                user_dataframe['file7'][2]['Canopy_Original_Bid'] = user_dataframe['file7'][2]['Max Bid']

                func2 = np.vectorize(calculate_bid)
                user_dataframe['file7'][2]['Max Bid'] = user_dataframe['file7'][2].apply(
                    lambda x: x['Max Bid'] * calculate_bid(x['Canopy_Bid_Tier'], user_input), axis=1)
                if (len(user_dataframe['file45'][2]) > 0):
                    user_dataframe['file7'][2] = fetch_45day_details(user_dataframe['file7'][2],
                                                                     user_dataframe['file45'][2])
                else:
                    user_dataframe['file7'][2]['Canopy_45_Day_CPC'] = 0
                    user_dataframe['file7'][2]['Canopy_45_Day_ACoS'] = 0

                user_dataframe['file7'][2] = new_bid_calculation(user_dataframe['file7'][2], user_input)

                user_dataframe['file7'][2] = user_dataframe['file7'][2].loc[
                    user_dataframe['file7'][2]['Record Type'] == 'Keyword']
                user_dataframe['file7'][2] = max_bid_bound(user_dataframe['file7'][2], constants.lower_bound_brands,
                                                           user_input)
                user_dataframe['file7'][2]['Bid Variance'] = (user_dataframe['file7'][2]['Max Bid'] -user_dataframe['file7'][2]['Canopy_Original_Bid']) / user_dataframe['file7'][2]['Canopy_Original_Bid']
                user_dataframe['file7'][2]['Bid Variance'] = round(user_dataframe['file7'][2]['Bid Variance'], 2)
                user_dataframe['file7'][2].loc[~np.isfinite(user_dataframe['file7'][2]['Bid Variance']), 'Bid Variance'] = 'Original Bid is 0'
            else:
                pass

            if is_European1:
                if len(user_dataframe['file7'][2]) > 0:
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column].astype('float64')
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column] * 100
                        user_dataframe['file7'][2][column] = round(user_dataframe['file7'][2][column], 2)
                    user_dataframe['file7'][2] = common_modules.decimal2comma(user_dataframe['file7'][2],
                                                                              ['Max Bid', 'Sales', 'Spend', 'ACoS',
                                                                               'Target_ACoS',
                                                                               'Canopy_Original_Bid',
                                                                               'Canopy_45_Day_ACoS',
                                                                               'Canopy_45_Day_CPC',
                                                                               'Canopy_45_Day_7_Day_CPC'])
                    user_dataframe['file7'][2]['Canopy_45_Day_7_Day_CPC'] = user_dataframe['file7'][1][
                        'Canopy_45_Day_7_Day_CPC'].str.replace('nan', ' ')
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column].astype('str') + "%"
                else:
                    pass
            else:
                if len(user_dataframe['file7'][2]) > 0:
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column].astype('float64')
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column] * 100
                        user_dataframe['file7'][2][column] = round(user_dataframe['file7'][2][column], 2)
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column].astype('str') + "%"
                else:
                    pass
        else:
            pass

    elif 'SponsoredProductsCampaigns1' and 'SponsoredProductsCampaigns2' and 'SponsoredBrandsCampaigns1' and 'SponsoredBrandsCampaigns2' in sheets:
        if len(user_dataframe['file7'][0]) > 0:
            is_European0 = user_dataframe['file7'][0][user_dataframe['file7'][0]['Max Bid'].apply(common_modules.is_european_format)][
                'Max Bid'].count() > 0
            if is_European0:
                user_dataframe['file7'][0] = common_modules.comma2decimal(user_dataframe['file7'][0],
                                                                          ['Max Bid', 'Sales', 'Spend'])
                user_dataframe['file7'][1] = common_modules.comma2decimal(user_dataframe['file7'][1],
                                                                          ['Max Bid', 'Sales', 'Spend'])
                for columns in ['Max Bid', 'Sales', 'Spend']:
                    user_dataframe['file7'][0][columns] = user_dataframe['file7'][0][columns].astype('float64')
                    user_dataframe['file7'][1][columns] = user_dataframe['file7'][1][columns].astype('float64')
            else:
                pass

            if len(user_dataframe['file7'][0]) > 0:
                user_dataframe['file7'][0] = max_bid_products(user_dataframe['file7'][0],
                                                              constants.record_types_product,
                                                              constants.status)
                user_dataframe['file7'][1] = max_bid_products(user_dataframe['file7'][1],
                                                              constants.record_types_product,
                                                              constants.status)

                # print("Level 1 Max Bid Initial", user_dataframe['file7'][0]['Max Bid'])
                user_dataframe['file7'][0] = determine_target_acos_products(user_dataframe['file7'][0],
                                                                            custom_dataframe, user_input['target_acos'])
                user_dataframe['file7'][1] = determine_target_acos_products(user_dataframe['file7'][1],
                                                                            custom_dataframe, user_input['target_acos'])
                func1 = np.vectorize(set_bid_tier)

                user_dataframe['file7'][0]['Canopy_Bid_Tier'] = func1(user_dataframe['file7'][0]['Orders'],
                                                                      user_dataframe['file7'][0]['ACoS'],
                                                                      user_dataframe['file7'][0]['Impressions'],
                                                                      user_dataframe['file7'][0]['Spend'],
                                                                      user_dataframe['file7'][0]['Target_ACoS'],
                                                                      constants.default_data, user_input)
                user_dataframe['file7'][1]['Canopy_Bid_Tier'] = func1(user_dataframe['file7'][1]['Orders'],
                                                                      user_dataframe['file7'][1]['ACoS'],
                                                                      user_dataframe['file7'][1]['Impressions'],
                                                                      user_dataframe['file7'][1]['Spend'],
                                                                      user_dataframe['file7'][1]['Target_ACoS'],
                                                                      constants.default_data, user_input)

                # print("Level 2 Bid Tier", user_dataframe['file7'][0].loc[user_dataframe['file7'][0]['Canopy_Bid_Tier'].isin(constants.poor_performer)]['Max Bid'].value_counts())
                common_modules.update_processing_tasks(task_id, 'Classifying each row into Bid Tier', 50)
                user_dataframe['file7'][0]['Canopy_Original_Bid'] = user_dataframe['file7'][0]['Max Bid']
                user_dataframe['file7'][1]['Canopy_Original_Bid'] = user_dataframe['file7'][1]['Max Bid']
                func2 = np.vectorize(calculate_bid)
                user_dataframe['file7'][0]['Max Bid'] = user_dataframe['file7'][0].apply(
                    lambda x: x['Max Bid'] * calculate_bid(x['Canopy_Bid_Tier'], user_input), axis=1)
                user_dataframe['file7'][1]['Max Bid'] = user_dataframe['file7'][1].apply(
                    lambda x: x['Max Bid'] * calculate_bid(x['Canopy_Bid_Tier'], user_input), axis=1)

                # print("Level 3 Bid Calculate", user_dataframe['file7'][0].loc[user_dataframe['file7'][0]['Canopy_Bid_Tier'].isin(constants.poor_performer)]['Max Bid'].value_counts())
                # print("USer input is", user_input)

                # print("Level 4 New column Max Bid Calculate", user_dataframe['file7'][0].loc[user_dataframe['file7'][0]['Canopy_Bid_Tier'].isin(constants.poor_performer)]['Max Bid'].value_counts())

                if (len(user_dataframe['file45'][0]) > 0):
                    user_dataframe['file7'][0] = fetch_45day_details(user_dataframe['file7'][0],
                                                                     user_dataframe['file45'][0])
                    user_dataframe['file7'][1] = fetch_45day_details(user_dataframe['file7'][1],
                                                                     user_dataframe['file45'][1])
                    common_modules.update_processing_tasks(task_id, 'Fetching ACOS and CPC from 45 day file ', 60)
                else:
                    user_dataframe['file7'][0]['Canopy_45_Day_CPC'] = 0
                    user_dataframe['file7'][0]['Canopy_45_Day_ACoS'] = 0
                    user_dataframe['file7'][1]['Canopy_45_Day_CPC'] = 0
                    user_dataframe['file7'][1]['Canopy_45_Day_ACoS'] = 0

                # user_dataframe['file7'][0]['Max Bid'] = user_dataframe['file7'][0]['Max Bid']
                user_dataframe['file7'][0] = new_bid_calculation(user_dataframe['file7'][0], user_input)
                user_dataframe['file7'][1] = new_bid_calculation(user_dataframe['file7'][1], user_input)

                # print((user_dataframe['file7'][0]['Max Bid']<=0.02).sum())
                user_dataframe['file7'][0] = max_bid_bound(user_dataframe['file7'][0], constants.lower_bound_products,
                                                           user_input)
                user_dataframe['file7'][1] = max_bid_bound(user_dataframe['file7'][1], constants.lower_bound_products,
                                                           user_input)
                user_dataframe['file7'][0]['Bid Variance'] = (user_dataframe['file7'][0]['Max Bid'] -user_dataframe['file7'][0]['Canopy_Original_Bid']) / user_dataframe['file7'][0]['Canopy_Original_Bid']
                user_dataframe['file7'][1]['Bid Variance'] = (user_dataframe['file7'][1]['Max Bid'] - user_dataframe['file7'][1]['Canopy_Original_Bid']) / user_dataframe['file7'][1]['Canopy_Original_Bid']
                user_dataframe['file7'][0]['Bid Variance'] = round(user_dataframe['file7'][0]['Bid Variance'], 2)
                user_dataframe['file7'][1]['Bid Variance'] = round(user_dataframe['file7'][1]['Bid Variance'], 2)
                user_dataframe['file7'][0].loc[~np.isfinite(user_dataframe['file7'][0]['Bid Variance']), 'Bid Variance'] = 'Original Bid is 0'
                user_dataframe['file7'][1].loc[~np.isfinite(user_dataframe['file7'][1]['Bid Variance']), 'Bid Variance'] = 'Original Bid is 0'
                # print((user_dataframe['file7'][0]['Max Bid']==0.02).sum())

            else:
                pass

            if is_European0:
                if len(user_dataframe['file7'][0]) > 0:
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column].astype('float64')
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column] * 100
                        user_dataframe['file7'][0][column] = round(user_dataframe['file7'][0][column], 2)
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column].astype('float64')
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column] * 100
                        user_dataframe['file7'][1][column] = round(user_dataframe['file7'][1][column], 2)
                    user_dataframe['file7'][0] = common_modules.decimal2comma(user_dataframe['file7'][0],
                                                                              ['Max Bid', 'Sales', 'Spend', 'ACoS',
                                                                               'Target_ACoS',
                                                                               'Canopy_Original_Bid',
                                                                               'Canopy_45_Day_ACoS',
                                                                               'Canopy_45_Day_CPC',
                                                                               'Canopy_45_Day_7_Day_CPC'])
                    user_dataframe['file7'][1] = common_modules.decimal2comma(user_dataframe['file7'][1],
                                                                              ['Max Bid', 'Sales', 'Spend', 'ACoS',
                                                                               'Target_ACoS',
                                                                               'Canopy_Original_Bid',
                                                                               'Canopy_45_Day_ACoS',
                                                                               'Canopy_45_Day_CPC',
                                                                               'Canopy_45_Day_7_Day_CPC'])
                    user_dataframe['file7'][0]['Canopy_45_Day_7_Day_CPC'] = user_dataframe['file7'][0][
                        'Canopy_45_Day_7_Day_CPC'].str.replace('nan', ' ')
                    user_dataframe['file7'][1]['Canopy_45_Day_7_Day_CPC'] = user_dataframe['file7'][1][
                        'Canopy_45_Day_7_Day_CPC'].str.replace('nan', ' ')

                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column].astype('str') + "%"
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column].astype('str') + "%"
                else:
                    pass
            else:
                if len(user_dataframe['file7'][0]) > 0:
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column].astype('float64')
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column] * 100
                        user_dataframe['file7'][0][column] = round(user_dataframe['file7'][0][column], 2)
                        user_dataframe['file7'][0][column] = user_dataframe['file7'][0][column].astype('str') + "%"
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column].astype('float64')
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column] * 100
                        user_dataframe['file7'][1][column] = round(user_dataframe['file7'][1][column], 2)
                        user_dataframe['file7'][1][column] = user_dataframe['file7'][1][column].astype('str') + "%"
                else:
                    pass

        else:
            pass
        if len(user_dataframe['file7'][2]) > 0:
            is_European1 = user_dataframe['file7'][2][user_dataframe['file7'][2]['Max Bid'].apply(common_modules.is_european_format)][
                'Max Bid'].count() > 0
            if is_European1:
                user_dataframe['file7'][2] = common_modules.comma2decimal(user_dataframe['file7'][2],
                                                                          ['Max Bid', 'Sales', 'Spend'])
                user_dataframe['file7'][3] = common_modules.comma2decimal(user_dataframe['file7'][3],
                                                                          ['Max Bid', 'Sales', 'Spend'])
                for columns in ['Max Bid', 'Sales', 'Spend']:
                    user_dataframe['file7'][2][columns] = user_dataframe['file7'][2][columns].astype('float64')
                    user_dataframe['file7'][3][columns] = user_dataframe['file7'][3][columns].astype('float64')
            else:
                pass
            user_dataframe['file7'][2] = max_bid_brands(user_dataframe['file7'][2], constants.record_types_product,
                                                        constants.status)
            user_dataframe['file7'][3] = max_bid_brands(user_dataframe['file7'][3], constants.record_types_product,
                                                        constants.status)

            if len(user_dataframe['file7'][2]) > 0:
                user_dataframe['file7'][2] = determine_target_acos_brands(user_dataframe['file7'][2], custom_dataframe,
                                                                          user_input['target_acos'])
                user_dataframe['file7'][3] = determine_target_acos_brands(user_dataframe['file7'][3], custom_dataframe,
                                                                          user_input['target_acos'])
                func1 = np.vectorize(set_bid_tier)

                user_dataframe['file7'][2]['Canopy_Bid_Tier'] = func1(user_dataframe['file7'][2]['Orders'],
                                                                      user_dataframe['file7'][2]['ACoS'],
                                                                      user_dataframe['file7'][2]['Impressions'],
                                                                      user_dataframe['file7'][2]['Spend'],
                                                                      user_dataframe['file7'][2]['Target_ACoS'],
                                                                      constants.default_data, user_input)
                user_dataframe['file7'][3]['Canopy_Bid_Tier'] = func1(user_dataframe['file7'][3]['Orders'],
                                                                      user_dataframe['file7'][3]['ACoS'],
                                                                      user_dataframe['file7'][3]['Impressions'],
                                                                      user_dataframe['file7'][3]['Spend'],
                                                                      user_dataframe['file7'][3]['Target_ACoS'],
                                                                      constants.default_data, user_input)
                user_dataframe['file7'][2]['Canopy_Original_Bid'] = user_dataframe['file7'][2]['Max Bid']
                user_dataframe['file7'][3]['Canopy_Original_Bid'] = user_dataframe['file7'][3]['Max Bid']

                func2 = np.vectorize(calculate_bid)
                user_dataframe['file7'][2]['Max Bid'] = user_dataframe['file7'][2].apply(
                    lambda x: x['Max Bid'] * calculate_bid(x['Canopy_Bid_Tier'], user_input), axis=1)
                user_dataframe['file7'][3]['Max Bid'] = user_dataframe['file7'][3].apply(
                    lambda x: x['Max Bid'] * calculate_bid(x['Canopy_Bid_Tier'], user_input), axis=1)
                if (len(user_dataframe['file45'][2]) > 0):
                    user_dataframe['file7'][2] = fetch_45day_details(user_dataframe['file7'][2],
                                                                     user_dataframe['file45'][2])
                    user_dataframe['file7'][3] = fetch_45day_details(user_dataframe['file7'][3],
                                                                     user_dataframe['file45'][3])

                else:
                    user_dataframe['file7'][2]['Canopy_45_Day_CPC'] = 0
                    user_dataframe['file7'][2]['Canopy_45_Day_ACoS'] = 0
                    user_dataframe['file7'][3]['Canopy_45_Day_CPC'] = 0
                    user_dataframe['file7'][3]['Canopy_45_Day_ACoS'] = 0

                user_dataframe['file7'][2] = new_bid_calculation(user_dataframe['file7'][2], user_input)
                user_dataframe['file7'][3] = new_bid_calculation(user_dataframe['file7'][3], user_input)

                user_dataframe['file7'][2] = user_dataframe['file7'][2].loc[
                    user_dataframe['file7'][2]['Record Type'] == 'Keyword']
                user_dataframe['file7'][3] = user_dataframe['file7'][3].loc[
                    user_dataframe['file7'][3]['Record Type'] == 'Keyword']
                user_dataframe['file7'][2] = max_bid_bound(user_dataframe['file7'][2], constants.lower_bound_brands,
                                                           user_input)
                user_dataframe['file7'][3] = max_bid_bound(user_dataframe['file7'][3], constants.lower_bound_brands,
                                                           user_input)
                user_dataframe['file7'][2]['Bid Variance'] = (user_dataframe['file7'][2]['Max Bid'] -user_dataframe['file7'][2]['Canopy_Original_Bid']) /  user_dataframe['file7'][2]['Canopy_Original_Bid']
                user_dataframe['file7'][3]['Bid Variance'] = (user_dataframe['file7'][3]['Max Bid'] -user_dataframe['file7'][3]['Canopy_Original_Bid']) / user_dataframe['file7'][3]['Canopy_Original_Bid']
                user_dataframe['file7'][2]['Bid Variance'] = round(user_dataframe['file7'][2]['Bid Variance'], 2)
                user_dataframe['file7'][3]['Bid Variance'] = round(user_dataframe['file7'][3]['Bid Variance'], 2)
                user_dataframe['file7'][2].loc[~np.isfinite(user_dataframe['file7'][2]['Bid Variance']), 'Bid Variance'] = 'Original Bid is 0'
                user_dataframe['file7'][3].loc[~np.isfinite(user_dataframe['file7'][3]['Bid Variance']), 'Bid Variance'] = 'Original Bid is 0'
            else:
                pass

            if is_European1:
                if len(user_dataframe['file7'][2]) > 0:
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column].astype('float64')
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column] * 100
                        user_dataframe['file7'][2][column] = round(user_dataframe['file7'][2][column], 2)
                        user_dataframe['file7'][3][column] = user_dataframe['file7'][3][column].astype('float64')
                        user_dataframe['file7'][3][column] = user_dataframe['file7'][3][column] * 100
                        user_dataframe['file7'][3][column] = round(user_dataframe['file7'][3][column], 2)
                    user_dataframe['file7'][2] = common_modules.decimal2comma(user_dataframe['file7'][2],
                                                                              ['Max Bid', 'Sales', 'Spend', 'ACoS',
                                                                               'Target_ACoS',
                                                                               'Canopy_Original_Bid',
                                                                               'Canopy_45_Day_ACoS',
                                                                               'Canopy_45_Day_CPC',
                                                                               'Canopy_45_Day_7_Day_CPC'])
                    user_dataframe['file7'][3] = common_modules.decimal2comma(user_dataframe['file7'][3],
                                                                              ['Max Bid', 'Sales', 'Spend', 'ACoS',
                                                                               'Target_ACoS',
                                                                               'Canopy_Original_Bid',
                                                                               'Canopy_45_Day_ACoS',
                                                                               'Canopy_45_Day_CPC',
                                                                               'Canopy_45_Day_7_Day_CPC'])
                    user_dataframe['file7'][2]['Canopy_45_Day_7_Day_CPC'] = user_dataframe['file7'][2][
                        'Canopy_45_Day_7_Day_CPC'].str.replace('nan', ' ')
                    user_dataframe['file7'][3]['Canopy_45_Day_7_Day_CPC'] = user_dataframe['file7'][3][
                        'Canopy_45_Day_7_Day_CPC'].str.replace('nan', ' ')
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column].astype('str') + "%"
                        user_dataframe['file7'][3][column] = user_dataframe['file7'][3][column].astype('str') + "%"
                else:
                    pass
            else:
                if len(user_dataframe['file7'][2]) > 0:
                    for column in ['ACoS', 'Target_ACoS', 'Canopy_45_Day_ACoS', 'Bid Variance']:
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column].astype('float64')
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column] * 100
                        user_dataframe['file7'][2][column] = round(user_dataframe['file7'][2][column], 2)
                        user_dataframe['file7'][2][column] = user_dataframe['file7'][2][column].astype('str') + "%"
                        user_dataframe['file7'][3][column] = user_dataframe['file7'][3][column].astype('float64')
                        user_dataframe['file7'][3][column] = user_dataframe['file7'][3][column] * 100
                        user_dataframe['file7'][3][column] = round(user_dataframe['file7'][3][column], 2)
                        user_dataframe['file7'][3][column] = user_dataframe['file7'][3][column].astype('str') + "%"
                else:
                    pass
        else:
            pass

    #user_dataframe['file7'][1]['Canopy_45_Day_7_Day_CPC'].fillna(' ', inplace=True)
    write_file(user_dataframe, task_id)
    common_modules.update_processing_tasks(task_id, 'Preparing files for download', 80)
    return task_id, 200

