from werkzeug.exceptions import HTTPException


class InternalServerError(HTTPException):
    description = (
        "Something went wrong"
    )
    code = 500


class SchemaValidationError(HTTPException):
    description = (
        "SchemaValidationError"
    )
    code = 400


class EmailAlreadyExistsError(Exception):
    description = (
        "User with given email address already exists"
    )
    code = 400


class UnauthorizedError(Exception):
    description = (
        "Invalid username or password"
    )
    code = 401


class EmailDoesNotExistsError(Exception):
    description = (
        "Couldn't find the user with given email address"
    )
    code = 400


class BadTokenError(Exception):
    description = (
        "Invalid token"
    )
    code = 403


class JSONBadRequestError(HTTPException):
    """
        Simply JSON Bad Request Error
        Invalid JSON format or there is not JSON when it is required
    """
    description = (
        "JSON Bad Request Error"
    )
    code = 403


class InvalidRequest(HTTPException):
    """
        General exception for a bad request
    """
    description = (
        "Invalid Request"
    )
    code = 400

class InvalidForm(HTTPException):
    """
        The request.form values are invalids
    """
    description = (
        "Invalid form"
    )
    code = 400
