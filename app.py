import os
from functools import wraps
from flask import Flask, flash, request, redirect, render_template,session,send_from_directory
from werkzeug.utils import secure_filename
from common import constants, common_modules
from errors import *
from script_v1 import top_level_flow, get_downloaded_files
import uuid
import logging
from datetime import datetime


UPLOAD_FOLDER = constants.upload_folder
DOWNLOAD_FOLDER = constants.download_folder
ALLOWED_EXTENSIONS = constants.allowed_extensions

app = Flask(__name__)
app.secret_key = uuid.uuid4().hex
tasks_data = constants.task_data

logging_time = datetime.now().strftime("%d-%m-%Y")
log_filename_debug = 'logs/' + 'bidapp' + logging_time + '.log'
logging.basicConfig(filename=log_filename_debug, level=logging.INFO)


def allowed_file(filename):
    """"To check the extension of the files being uploaded"""
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def validate_taskid_json(f):
    @wraps(f)
    def wrapper(*args, **kw):
        json = request.get_json()
        if not ('task_id' in json):
            raise JSONBadRequestError
        task_id = json['task_id']
        if not (task_id in tasks_data):
            raise JSONBadRequestError
        return f(*args, **kw)
    return wrapper

def validate_post_json(f):
    @wraps(f)
    def wrapper(*args, **kw):
        if not request.is_json:
            raise JSONBadRequestError
        return f(*args, **kw)
    return wrapper


@app.route('/')
def home():
    """Home page which shows the form"""
    return render_template('v3.html')

@app.route('/process/', methods=['GET','POST'])
def process():
    """Fetch data entered by user throgh UI. """
    unique_user = str(uuid.uuid4()) # This creates a session id for each user
    session['user_id'] = unique_user
    logging.info("Session id is '{0}'".format(session['user_id']))
    files = {}
    files['file45'] = request.files['firstFile']
    files['file7'] = request.files['secondFile']
    files['custom'] = request.files['thirdFile']
    if ((files['file45'].filename == '') & (files['file7'].filename == '')):
        flash('Both files are required')
        return render_template('v3.html')
    for key, value in files.items():
        _, ext = os.path.splitext(value.filename)
        updated_file = secure_filename(f'{unique_user}_{key}_{ext}')
        if updated_file and allowed_file(value.filename):
            value.save(os.path.join(UPLOAD_FOLDER, updated_file))
    target_acos = request.form['acos']
    upper_bound = request.form['upper_bound']
    upper_bound = upper_bound if len(upper_bound)==0 else float(upper_bound)
    poor_performer = request.form['poor_performer']
    under_invested = request.form['under_invested']
    low_traffic = request.form['low_traffic']
    under_invested_bidadjust = request.form['under_invested_bidadjust']
    on_target_bidadjust = request.form['on_target_bidadjust']
    poor_performer_1_bidadjust = request.form['poor_performer_1_bidadjust']
    poor_performer_2_bidadjust = request.form['poor_performer_2_bidadjust']
    poor_performer_3_bidadjust = request.form['poor_performer_3_bidadjust']
    poor_performer_4_bidadjust = request.form['poor_performer_4_bidadjust']
    not_validate_bidadjust = request.form['not_validate_bidadjust']
    low_traffic_1_bidadjust = request.form['low_traffic_1_bidadjust']
    low_traffic_2_bidadjust = request.form['low_traffic_2_bidadjust']
    over_optimized_bidadjust = request.form['over_optimized_bidadjust']

    data = {'f45': files['file45'], 'f7': files['file7'], 'custom': files['custom'], 'target_acos': float(target_acos) / 100,
            'poor_performer': float(poor_performer), 'under_invested': float(under_invested),
            'low_traffic': float(low_traffic),'upper_bound':(upper_bound),
            'under_invested_bidadjust':float(under_invested_bidadjust)/100, 'on_target_bidadjust':float(on_target_bidadjust)/100,
            'poor_performer_1_bidadjust':float(poor_performer_1_bidadjust)/100,'poor_performer_2_bidadjust':float(poor_performer_2_bidadjust)/100,
            'poor_performer_3_bidadjust': float(poor_performer_3_bidadjust)/100,'poor_performer_4_bidadjust':float(poor_performer_4_bidadjust)/100,
            'not_validate_bidadjust': float(not_validate_bidadjust)/100, 'low_traffic_1_bidadjust':float(low_traffic_1_bidadjust)/100,
            'low_traffic_2_bidadjust': float(low_traffic_2_bidadjust)/100, 'over_optimized_bidadjust':float(over_optimized_bidadjust)/100
            }
    #print(data)
    logging.info("Data from form collected '{0}' ".format(data))
    tasks_data[session['user_id']] = data
    # the server shows you the process page (this occurs after the client submits the form data)
    return render_template('process.html', task_id=session['user_id'], total=2)

@app.route('/process/start/', methods=['GET','POST'])
@validate_post_json
@validate_taskid_json
def start():
    """Starts a job by the task_id
    This must be called by the process page"""
    user_inputs=tasks_data[session['user_id']]
    task_id = session['user_id']
    return top_level_flow(task_id,user_inputs)

@app.route('/process/percentage/', methods=['POST'])
@validate_post_json
@validate_taskid_json
def percentage():
    """For updating the progress bar"""
    json = request.get_json()
    return constants.processing_tasks[json['task_id']], 200

@app.route('/success/<string:task_id>')
def success(task_id):
    """Fetch the files corresponding to each user """
    #print(constants.processing_tasks[task_id])
    session.pop('user_id', None)
    downloaded_files = get_downloaded_files(DOWNLOAD_FOLDER,task_id)
    file1 = downloaded_files['file7'].split('/')[-1]
    file2 = downloaded_files['ini'].split('/')[-1]
    common_modules.update_processing_tasks(task_id, 'Files are ready to be downloaded', 100)
    logging.info("Files are ready to be downloaded")
    return render_template('success.html', file1=file1, file2=file2)

@app.route('/download/<string:filename>')
def download(filename):
    """"Downloads both INI and 7 day bulk file after processing"""
    #return send_from_directory(os.path.join(os.getcwd(), DOWNLOAD_FOLDER), filename)
    file_path_dowload = os.path.join(os.getcwd(), DOWNLOAD_FOLDER , filename)
    file_handle = open(file_path_dowload, 'rb')
    def stream_and_remove_file():
        yield from file_handle
        file_handle.close()
        os.remove(file_path_dowload)
    return app.response_class(
            stream_and_remove_file(),
            mimetype='text/xlsx',
            headers={'Content-Disposition': 'attachment', 'filename': filename,}
    )

if __name__ == '__main__':
    #app.debug = True
    app.run(host="0.0.0.0", debug=True, port="5000")



