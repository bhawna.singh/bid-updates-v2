from common import constants
from flask import jsonify
from functools import wraps
import pandas as pd
import timeit,gc
import logging
import warnings
warnings.filterwarnings('ignore')

#percent to float convertor
def percent2float(x):
    y=x
    z=str(y).split(',')
    #print(y)
    if (len(z)>1):
        #print("I am comma")
        x=str(x).replace(",",".")
        return float(x.strip('%'))/100
    else:
        #print("Typpe", type(x))
        #print("XXXXXX",x)
        return float(str(x.strip('%')))/100

def update_processing_tasks(task_id, title, percentage):
    """Updates the data of the job corresponding to the task_id"""
    constants.processing_tasks[task_id] = jsonify({'title': title, 'percentage': percentage})

def is_european_format(x):
    y=str(x).split(',')
    if (len(y)>1):
        return True
    else:
        return False

def comma2decimal(df, columns):
    for column in columns:
        df[column] = df[column].str.replace(",",".")
    return df

def decimal2comma(df, columns):
    for column in columns:
        #print(df[column])
        df[column] = df[column].astype('str').str.replace(".",",")
        #print(df[column])
    return df

def MeasureTime(f):
    @wraps(f)
    def _wrapper(*args, **kwargs):
        gcold = gc.isenabled()
        gc.disable()
        start_time = timeit.default_timer()
        try:
            result = f(*args, **kwargs)
        finally:
            elapsed = timeit.default_timer() - start_time
            if gcold:
                gc.enable()
            logging.info('Function "{}": {}s'.format(f.__name__, elapsed))
        return result
    return _wrapper

class MeasureBlockTime:
    def __init__(self,name="(block)", no_print = False, disable_gc = True):
        self.name = name
        self.no_print = no_print
        self.disable_gc = disable_gc
    def __enter__(self):
        if self.disable_gc:
            self.gcold = gc.isenabled()
            gc.disable()
        self.start_time = timeit.default_timer()
    def __exit__(self,ty,val,tb):
        self.elapsed = timeit.default_timer() - self.start_time
        if self.disable_gc and self.gcold:
            gc.enable()
        if not self.no_print:
            print('Function "{}": {}s'.format(self.name, self.elapsed))
        return False #re-raise any exceptions



