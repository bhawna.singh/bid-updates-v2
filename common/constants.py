#Columns for config file creation
col_name = ["Record Type", "Max Bid", "Match Type", "Campaign Status", "Ad Group Status", "Status"]
sheets = ['Sponsored Products Campaigns', 'Sponsored Brands Campaigns',
          'Sponsored Products Campaigns 1','Sponsored Products Campaigns 2',
          'Sponsored Brands Campaigns 1','Sponsored Brands Campaigns 2']


#Default data for Set_Bid_Tier
default_data = {
    'category_bound_on_target': 0.03,
    'category_bound_poorperf_1': 0.05,
    'category_bound_poorperf_2': 0.10,
    'category_bound_poorperf_3': 0.20,
    'category_bound_poorperf_4': 0.50,
    'category_bound_over_optimized': 0.03,
    'over_optimized_bidadjust': -0.05,
    'poor_performer':5,
    'under_invested':2,
    'low_traffic':70
}

# Tiers

tiers = {
    'tier_under_invested': 0.10,
    'tier_on_target': 0.0,
    'tier_poorperf-1': -0.08,
    'tier_poorperf-2': -0.15,
    'tier_poorperf-3': -0.25,
    'tier_poorperf-4': -0.50,
    'tier_not_validated': 0.05,
    'tier_low_traffic-1': 0.03,
    'tier_low_traffic-2': 0.00,
    'tier_no_change': 0.00
}

#Target Acos
target_acos = 0.2

#For Max Bid insertion
record_types_product = ['product targeting','keyword','ad group']
record_types_brands = ['keyword']
status = ['paused', 'rejected', 'archived', 'drafts', 'draft', 'running']

#Upload Folder
upload_folder = 'UploadedFiles'
download_folder = 'DownloadedFiles'

#For max_biid re calculation
poor_performer = ['tier_poorperf-1','tier_poorperf-2','tier_poorperf-3','tier_poorperf-4']

#allowed extensions
allowed_extensions = {'xlsx'}

#For dynamic web pages
processing_tasks = {}
download_data = {}
task_data = {}

lower_bound_products=0.02
lower_bound_brands=0.10




